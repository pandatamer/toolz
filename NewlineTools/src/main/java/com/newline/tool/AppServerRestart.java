package com.newline.tool;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

public class AppServerRestart {
	public static final String AZURE_BASE_URL = "httpsmanagement.azure.com/subscriptions/08b67d18-8f1a-4e79-a7a8-ac09b757fea5/resourceGroups/platform/providers/Microsoft.Web/sites/newline-portal/slots/";
	public static final String API_VERSION = "2015-08-01";
	public static final String OAUTH_TOKEN_URL = "https://login.microsoftonline.com/710c3e08-c6d7-437f-8452-a7e3ed2133b8/oauth2/token";
	public static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
	public static final String GRANT_TYPE = "client_credentials";
	public static final String CLIENT_ID = "771af91-4eb0-af52-556a5f78e0f0";
	public static final String CLIENT_SECRET = "3uH+FJ9axQtUHpabuVzgYrKnwOgEZY1LLxb88SkgOPU=";
	public static final String RESOURCE = "https://management.azure.com/";
	
	public static String buildName;
	public static String action;
	//WT - WaitTime (in mins)
	public static int afterStopWT = 5;
	public static int afterStartWT = 10;
	String bearerToken;
	
	public static void main(String[] args) throws Exception {
		if(args.length == 0 || args[0] == null) {
			throw new Exception("Argument 'BuildName' is not passed, hence stoping this restart process.");
		}
		buildName = args[0];
		//System.out.println("BuildName: " + buildName);
		if(args.length == 2 && args[1] != null) {
			action = args[1];
		}
		AppServerRestart appServerRestart = new AppServerRestart();
		if("stop".equalsIgnoreCase(action)) {
			appServerRestart.doStopServer();
		} else if ("start".equalsIgnoreCase(action)) {
			appServerRestart.doStartServer();
		} else {
			appServerRestart.restartServer();
		}
	}
	
	public void doStopServer() throws Exception {
		System.out.println("Stoping '" + buildName + "' build...");
		getBearerToken();
		stopServer();
		TimeUnit.MINUTES.sleep(afterStopWT);
	}
	
	public void doStartServer() throws Exception {
		System.out.println("Starting '" + buildName + "' build...");
		getBearerToken();
		startServer();
		TimeUnit.MINUTES.sleep(afterStartWT);
	}

	public void restartServer() throws Exception {
		System.out.println("Restarting '" + buildName + "' build...");
		getBearerToken();
		stopServer();
		TimeUnit.MINUTES.sleep(afterStopWT);
		startServer();
		TimeUnit.MINUTES.sleep(afterStartWT);
	}

	// HTTP POST request to get bearer token
	private void getBearerToken() throws Exception {
		System.out.println("\nGetBearerToken>>>");
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(OAUTH_TOKEN_URL);
		post.setHeader("Content-Type", CONTENT_TYPE);
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("grant_type", GRANT_TYPE));
		urlParameters.add(new BasicNameValuePair("client_id", CLIENT_ID));
		urlParameters.add(new BasicNameValuePair("client_secret", CLIENT_SECRET));
		urlParameters.add(new BasicNameValuePair("resource", RESOURCE));
		post.setEntity(new UrlEncodedFormEntity(urlParameters));
		HttpResponse response = client.execute(post);
		System.out.println("Request to get BearerToken");
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		System.out.println("Response String: " + result);
		bearerToken = result.substring(result.lastIndexOf(":") + 1);
		bearerToken = bearerToken.substring(1, bearerToken.length() - 2);
		bearerToken = "Bearer " + bearerToken;
		System.out.println("Bearer Token: " + bearerToken);
	}

	// HTTP POST request to stop the Apache server
	private void stopServer() throws Exception {
		System.out.println("\nStopServer>>>");
		//String url = "https://management.azure.com/subscriptions/08b67d18-8f1a-4e79-a7a8-ac09b757fea5/resourceGroups/platform/providers/Microsoft.Web/sites/newline-portal/slots/" + buildName + "/stop?api-version=2015-08-01";
		StringBuilder url = new StringBuilder(AZURE_BASE_URL);
		url.append(buildName).append("/stop?api-version=").append(API_VERSION);
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url.toString());
		post.setHeader("Content-Type", CONTENT_TYPE);
		post.setHeader("Authorization", bearerToken);
		HttpResponse response = client.execute(post);
		System.out.println("Request to stop the server");
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		System.out.println("Response String: " + result);
	}

	// HTTP POST request to start the Apache server
	private void startServer() throws Exception {
		System.out.println("\nStartServer>>>");
		//String url = "https://management.azure.com/subscriptions/08b67d18-8f1a-4e79-a7a8-ac09b757fea5/resourceGroups/platform/providers/Microsoft.Web/sites/newline-portal/slots/" + buildName + "/start?api-version=2015-08-01";
		StringBuilder url = new StringBuilder(AZURE_BASE_URL);
		url.append(buildName).append("/start?api-version=").append(API_VERSION);
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url.toString());
		post.setHeader("Content-Type", CONTENT_TYPE);
		post.setHeader("Authorization", bearerToken);
		HttpResponse response = client.execute(post);
		System.out.println("Request to start the server");
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		//System.out.println("Response String: " + result.toString());
	}
}
